(ns core.keys
  (:require
   [clojure.string :as strings]))

(defn suppressed-key? [keycode]
  (not (or (strings/starts-with? keycode "F")
           (strings/starts-with? keycode "ContextMenu"))))

(defn produce-keypress-fn [on-key-fn]
  (fn [keyboard-event]
    (when-not (or (aget keyboard-event "altKey")
                  (aget keyboard-event "ctrlKey"))
      (let [keycode (aget keyboard-event "code")]
        (when (suppressed-key? keycode)
          (.preventDefault keyboard-event))
        (on-key-fn keycode)))))

(defn setup-button-control! [on-key-fn]
  (set! js/window.tetrisSubmitKey on-key-fn))

(defn setup-key-listener [& on-key-fns]
  (let [on-key-fn (fn [keycode]
                    (loop [[on-key-fn & remaining] (remove nil? on-key-fns)]
                      (when on-key-fn
                        (do (on-key-fn keycode)
                            (recur remaining)))))]
    (-> js/document
        (.getElementById "doc-body")
        (.addEventListener "keydown" (produce-keypress-fn on-key-fn)))
    (setup-button-control! on-key-fn)))
