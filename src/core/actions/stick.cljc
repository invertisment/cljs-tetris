(ns core.actions.stick)

(defn- put-piece [field piece]
  (persistent!
   (reduce
    (fn [field-2d {:keys [coord color]}]
      (let [[x y] coord]
        (assoc! field-2d y (assoc (get field-2d y) x color))))
    (transient field)
    piece)))

(defn stick-piece! [before-stick-fn {:keys [field piece] :as stateT}]
  (-> stateT
      before-stick-fn
      (dissoc! :piece :piece-bounds :piece-path)
      (assoc! :field (put-piece field piece))))
