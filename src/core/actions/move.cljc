(ns core.actions.move
  (:require [core.constants :as const]
            [core.actions.piece-ops :refer [piece-op-scalar get-piece-height set-piece-height]]
            [core.actions.rotate :as rot]
            [core.actions.stick :refer [stick-piece!]]
            [core.actions.piece-gen :refer [generate-new-piece]]
            [core.piece-validators :as v]
            [core.actions.clear-lines :refer [remove-full-lines!]]
            [core.actions.new-piece :refer [new-piece]]
            [core.actions.count-score :refer [count-score]]
            [core.ai.piece-path :as piece-path]))

(defn- stick-and-generate-new-piece [valid? update-score-fn before-move-fn before-stick-fn state]
  (->> state
       transient
       (stick-piece! before-stick-fn)
       remove-full-lines!
       persistent!
       (new-piece valid?)))

(defn right [valid? update-score-fn before-move-fn before-stick-fn state]
  (let [moved (piece-op-scalar inc identity before-move-fn state)]
    (when (valid? moved)
      moved)))

(defn left [valid? update-score-fn before-move-fn before-stick-fn state]
  (let [moved (piece-op-scalar dec identity before-move-fn state)]
    (when (valid? moved)
      moved)))

(defn down [valid? update-score-fn before-move-fn before-stick-fn state]
  (update-score-fn
   (count-score
    (let [moved (piece-op-scalar identity inc before-move-fn state)]
      (if (valid? moved)
        moved
        (stick-and-generate-new-piece valid? update-score-fn before-move-fn before-stick-fn state))))))

(defn ensure-hold-from-next [valid? update-score-fn state]
  (if (:hold-piece state)
    state
    (let [[next-piece & next-pieces] (:next-pieces state)
          current-piece (select-keys state (keys (first (:next-pieces state))))
          hold-state (assoc
                      state
                      :hold-piece next-piece
                      :next-pieces next-pieces)]
      (when (valid? hold-state)
        hold-state))))

(defn hold [valid? update-score-fn before-move-fn before-stick-fn state]
  (when-let [hold-state (ensure-hold-from-next valid? update-score-fn state)]
    (let [hold-piece (:hold-piece hold-state)
          current (select-keys hold-state (keys hold-piece))
          current-height (get-piece-height current)]
      (assoc
       (merge hold-state (set-piece-height before-move-fn hold-piece current-height))
       :hold-piece (set-piece-height before-move-fn current 0)))))

(defn rotate [valid? update-score-fn before-move-fn before-stick-fn state]
  (rot/rotate-piece-clockwise state))

(defn rotate-counter-clockwise [valid? update-score-fn before-move-fn before-stick-fn state]
  (rot/rotate-piece-counter-clockwise state))

(defn down-no-validation [before-move-fn state]
  (piece-op-scalar identity inc before-move-fn state))

(defn down-no-validation-by-number [before-move-fn state n]
  (piece-op-scalar identity #(+ % n) before-move-fn state))

(defn- bottom-iterative [update-score-fn before-move-fn state min-found-height max-found-height]
  (->> (iterate (partial down-no-validation before-move-fn) state)
       (take-while v/cheaper-field-valid?)
       last))

(defn- bottom-no-check [valid? update-score-fn before-move-fn before-stick-fn {:keys [height] :as state}]
  (down
   valid?
   update-score-fn
   before-move-fn before-stick-fn
   (bottom-iterative update-score-fn before-move-fn state 0 (dec height))))

(defn bottom [valid? update-score-fn before-move-fn before-stick-fn state]
  (when (:piece state)
    (bottom-no-check valid? update-score-fn before-move-fn before-stick-fn state)))

(defn new-field [next-pieces]
  (merge
   {:field (vec (repeat const/field-height const/empty-row))
    :next-pieces (rest next-pieces)
    :score {}
    :levels const/gravity-intervals
    :game-state :started
    :width const/field-width
    :height const/field-height}
   (first next-pieces)))

(defn new-game [valid? update-score-fn before-move-fn before-stick-fn state]
  (update-score-fn
   (merge
    state
    (new-field (repeatedly #(generate-new-piece const/pieces))))))

(defn gravity-down [& args]
  (apply down args))

(defn nop [& args])

(defn direction [const-value]
  (condp (fn [expected const-value]
           (expected const-value)) const-value
    const/rotate-clockwise #'rotate
    const/left #'left
    const/right #'right
    const/down #'down
    const/bottom #'bottom
    const/hold #'hold
    const/rotate-counter-clockwise #'rotate-counter-clockwise
    const/new-game #'new-game
    const/gravity-pull-down #'gravity-down
    #'nop))

(defn move [valid? update-score-fn before-move-fn before-stick-fn state key-code]
  (v/validate
   valid?
   ((direction key-code) valid? update-score-fn before-move-fn before-stick-fn state)))

(defn next-field-state [update-score-fn state key-code]
  (move v/field-valid? update-score-fn
        piece-path/before-move!
        piece-path/before-stick-piece!
        state
        key-code))
