(ns core.profiler.profile
  (:require
   [core.ai.core :as ai-core]
   [core.ai.genome :as genome]
   [core.ai.main :as main]
   [core.constants :as const]))

(defn run-task []
  (let [deserialized (main/deserialize)
        generation (or (:generation deserialized) 0)
        population-size (or (:population-size deserialized) 50)
        genomes (seq (mapv
                      (fn [genome] (genome/ensure-weight-existence const/used-genome-keys genome))
                      (:genomes deserialized)))
        calc-score-fn (genome/build-calculate-score-fn const/used-genome-keys genome/genome-coeff-fns)]
    (ai-core/train
     calc-score-fn
     generation
     200
     genomes
     100
     (constantly nil)
     pmap)))

(defn profile []
  (identity #_with-open #_[flames (flames/start! {:port 54321, :host "localhost"})]
   (let [f (future
             (println "start")
             (run-task)
             (println "done"))]
     (Thread/sleep 50000)
     (future-cancel f)
     (Thread/sleep 100)
     (println (future-cancelled? f)))))

;; https://github.com/jstepien/flames
;; http://localhost:54321/flames.svg
#_(def flames (flames/start! {:port 54321, :host "localhost" :dt 5 :load 0.05}))
#_(def flames (flames/start! {:port 54321, :host "localhost" :dt 2 :load 0.1}))
#_(flames/stop! flames)

#_(profile)

#_(run-task)
