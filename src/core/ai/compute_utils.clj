(ns core.ai.compute-utils
  (:require [clojure.core.async :refer [to-chan <!!] :as async])
  (:import me.tongfei.progressbar.ProgressBar))

(defn print-progress! [s]
  (print s)
  (flush))

(defn- f-rand-sleep! [f input]
  (Thread/sleep (+ (rand-int 1000) 1000))
  (f input))

(defn future-map [f li]
  (->> li
       (mapv #(future (f %)))
       doall
       (mapv deref)
       #_(map-indexed
          (fn [i ref]
            (let [out (deref ref)]
              (println "Finished" i)
              out)))))
#_(future-map inc [1 2 3 4 5])

(defn p-map [f li]
  (pmap f li))
#_(p-map (partial f-rand-sleep! inc) [1 2 3 4 5])

;; https://dzone.com/articles/clojure-concurrency-and-blocking-with-coreasync
;; Doesn't use multiple threads.
;; Go block pool can get exhausted on machines that have 8+ cores.
(defn back-pressure-map [f li]
  (let [input-ch-li [(to-chan li)]
        output-ch (async/merge
                   (map
                    (fn [_] (async/map f input-ch-li))
                    (range 20)))
        out (<!! (async/into () output-ch))]
    (println)
    out))
#_(back-pressure-map inc [1 2 3 4 5])

(defn map-w-progress [title f items]
  (with-open [pb (new ProgressBar title (count items))]
    (doall (future-map
            (fn [item]
              (let [output (f item)]
                (.step pb)
                output))
            items))))
#_(map-w-progress
   "title"
   #(do
      (Thread/sleep (rand-int 2000))
      (inc %))
   (range 10))
