(ns core.ai.placement
  (:require
   [core.actions.new-piece :as new-piece]
   [core.ai.moves :as moves]
   [core.ai.piece-path :as piece-path]
   [core.piece-validators :as v]))

(defn to-placement [state]
  {:path []
   :state state})

(defn calculate-score-for-single-piece-placement [calc-score-fn genome piece-placement]
  [(calc-score-fn genome piece-placement) piece-placement])

(defn pick-better-score [[score-a _move-a :as a] [score-b _move-b :as b]]
  (if (< score-a score-b)
    b
    a))

(defn calculate-scores [calc-score-fn genome piece-placements]
  (map
   (partial calculate-score-for-single-piece-placement calc-score-fn genome)
   piece-placements))

(defn find-ranked-piece-placements [before-move-fn before-stick-fn calc-score-fn genome placement]
  (->> placement
       (moves/find-piece-placements before-move-fn before-stick-fn)
       (calculate-scores calc-score-fn genome)))

(defn pick-best-state [pick-better-state-bifn ranked-piece-placements]
  (second (reduce
           pick-better-state-bifn
           (first ranked-piece-placements)
           (rest ranked-piece-placements))))

(defn internal-pick-best-1deep-piece-placement [pick-better-state-bifn before-move-fn before-stick-fn calc-score-fn genome placement]
  (->> placement
       (find-ranked-piece-placements before-move-fn before-stick-fn calc-score-fn genome)
       (pick-best-state pick-better-state-bifn)))

;; Computes with 1N but only one-piece deep
(defn pick-best-1deep-piece-placement [_is-game-ended-fn pick-better-state-bifn calc-score-fn genome state]
  (->> state
       to-placement
       (internal-pick-best-1deep-piece-placement
        pick-better-state-bifn
        piece-path/before-move-noop
        piece-path/before-stick-piece-noop
        calc-score-fn
        genome)))

(defn- place-next-piece [is-game-ended-fn pick-better-state-bifn before-move-fn before-stick-fn calc-score-fn genome {:keys [state] :as placement}]
  (if (is-game-ended-fn (:state placement))
    placement
    (let [next-placement (internal-pick-best-1deep-piece-placement
                          pick-better-state-bifn
                          before-move-fn before-stick-fn
                          calc-score-fn genome placement)]
      (assoc placement
             :state (:state next-placement)))))

;; Computes well but tries too many pieces (N^2) and isn't suitable for front-end (500ms in Java)
(defn pick-best-2deep-piece-placement [is-game-ended-fn pick-better-state-bifn calc-score-fn genome state]
  (->> (to-placement state)
       (moves/find-piece-placements piece-path/before-move-noop piece-path/before-stick-piece-noop)
       (mapv (fn [piece-placement]
               (->> piece-placement
                    (place-next-piece is-game-ended-fn pick-better-state-bifn
                                      piece-path/before-move-noop
                                      piece-path/before-stick-piece-noop
                                      calc-score-fn genome)
                    (calculate-score-for-single-piece-placement calc-score-fn genome))))
       (pick-best-state pick-better-state-bifn)))

(defn swap-next-piece [is-game-ended-fn {:keys [next-pieces] :as state}]
  (let [[next-piece] next-pieces
        piece-keys (keys next-piece)
        current-piece (select-keys state piece-keys)]
    (assoc (new-piece/new-piece v/field-valid? state)
           :next-pieces (cons current-piece next-pieces))))

;; 2N
(defn- place-two-pieces-returning-first-piece-coord-path [is-game-ended-fn pick-better-state-bifn before-move-fn before-stick-fn calc-score-fn genome placement first-move-placements]
  (if (is-game-ended-fn (:state placement))
    placement
    (let [first-placement (or (->> first-move-placements
                                   (calculate-scores calc-score-fn genome)
                                   (pick-best-state pick-better-state-bifn))
                              placement)]
      (if (is-game-ended-fn (:state first-placement))
        first-placement
        (assoc (assoc-in (or (internal-pick-best-1deep-piece-placement pick-better-state-bifn before-move-fn before-stick-fn calc-score-fn genome first-placement)
                             first-placement)
                         [:state :prev-piece-path]
                         (:prev-piece-path (:state first-placement)))
               :path (:path first-placement))))))

(defn- conflicting-placement? [piece-coord-path {:keys [state] :as placement}]
  (v/overlay? (:field state) piece-coord-path))

(defn- pick-non-conflicting-first-placement [pick-better-state-bifn piece-coord-path calc-score-fn genome first-move-placements]
  (->> first-move-placements
       (remove (partial conflicting-placement? piece-coord-path))
       (calculate-scores calc-score-fn genome)
       (pick-best-state pick-better-state-bifn)))

;; 2N of piece placements
(defn- swap-next-piece-and-place-two-pieces [is-game-ended-fn
                                             pick-better-state-bifn
                                             before-move-fn before-stick-fn
                                             before-move-fn-noop before-stick-fn-noop
                                             calc-score-fn
                                             genome
                                             {:keys [state] :as placement}
                                             first-move-placements]
  (if (is-game-ended-fn state)
    placement
    (let [swapped-state (swap-next-piece is-game-ended-fn state)]
      (if (is-game-ended-fn swapped-state)
        placement
        (let [swapped-placement (to-placement swapped-state)
              two-swapped-placement-with-first-prev-piece-path
              (place-two-pieces-returning-first-piece-coord-path
               is-game-ended-fn
               pick-better-state-bifn
               before-move-fn-noop before-stick-fn-noop
               calc-score-fn
               genome
               swapped-placement
               (moves/find-piece-placements before-move-fn before-stick-fn swapped-placement))]
          (assoc two-swapped-placement-with-first-prev-piece-path
                 :path
                 (:path (pick-non-conflicting-first-placement
                         pick-better-state-bifn
                         (-> two-swapped-placement-with-first-prev-piece-path
                             :state
                             :prev-piece-path)
                         calc-score-fn
                         genome first-move-placements))))))))

;; Takes about 4N piece lookups to compute; but also calculates piece paths
(defn pick-best-2deepcheap-piece-placement [is-game-ended-fn
                                            pick-better-state-bifn
                                            calc-score-fn
                                            genome
                                            state]
  (let [placement (to-placement state)
        first-move-placements (moves/find-piece-placements piece-path/before-move-noop piece-path/before-stick-piece-noop placement) ;; 1N
        swapped-piece-best-placement (swap-next-piece-and-place-two-pieces ;; 2N
                                      is-game-ended-fn
                                      pick-better-state-bifn
                                      piece-path/before-move-noop piece-path/before-stick-piece!
                                      piece-path/before-move-noop piece-path/before-stick-piece-noop
                                      calc-score-fn genome placement
                                      first-move-placements)
        best-1looktwice-placement (place-two-pieces-returning-first-piece-coord-path ;; 1N
                                   is-game-ended-fn
                                   pick-better-state-bifn
                                   piece-path/before-move-noop piece-path/before-stick-piece-noop
                                   calc-score-fn genome placement first-move-placements)]
    (if (> (calc-score-fn genome swapped-piece-best-placement)
           (calc-score-fn genome best-1looktwice-placement))
      swapped-piece-best-placement
      best-1looktwice-placement)))

(defn place-best-look1-piece [is-game-ended-fn pick-better-state-bifn calc-score-fn genome state]
  (:state (pick-best-1deep-piece-placement
           is-game-ended-fn
           pick-better-state-bifn
           calc-score-fn genome state)))

(defn place-best-look2-piece [is-game-ended-fn pick-better-state-bifn calc-score-fn genome state]
  (:state (pick-best-2deep-piece-placement is-game-ended-fn pick-better-state-bifn calc-score-fn genome state)))

(defn place-best-2deepcheap-piece [is-game-ended-fn pick-better-state-bifn calc-score-fn genome state]
  (:state (pick-best-2deepcheap-piece-placement is-game-ended-fn pick-better-state-bifn calc-score-fn genome state)))

(defn apply-pieces-while [is-game-ended-fn place-best-piece-fn pick-better-state-bifn calc-score-fn genome state]
  (loop [state state]
    (if (is-game-ended-fn state)
      state
      (recur (place-best-piece-fn is-game-ended-fn pick-better-state-bifn calc-score-fn genome state)))))
