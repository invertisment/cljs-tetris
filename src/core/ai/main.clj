(ns core.ai.main
  (:gen-class)
  (:require
   [clojure.java.io :as io]
   [clojure.java.shell :as sh]
   [clojure.string :as str]
   [core.ai.core :as ai-core]
   [core.ai.genome :as genome]
   [core.constants :as const]))

(defn get-git-revision []
  (->> (sh/sh "git" "rev-parse" "--short" "HEAD")
       :out
       str/trim))

(defn get-git-commit-count []
  (->> (sh/sh "git" "rev-list" "--count" "HEAD")
       :out
       str/trim))

(defn get-filename []
  (format "model-%s-%s.edn" (get-git-commit-count) (get-git-revision)))

(defn serialize-fn [max-generations population-size max-tetrominoes-count generation genomes]
  (spit
   (get-filename)
   {:generation generation
    :genomes genomes
    :max-generations max-generations
    :population-size population-size
    :max-tetrominoes-count max-tetrominoes-count}))

(defn deserialize []
  (let [filename (get-filename)]
    (println (format "Reading %s" filename))
    (when (.exists (io/file filename))
      (read-string (slurp filename)))))

(defn ensure-genomes [genome-li population-size]
  (let [genomes (seq (mapv
                      (fn [genome] (genome/ensure-weight-existence const/used-genome-keys genome))
                      genome-li))
        genome-count (count genomes)]
    (if (< genome-count population-size)
      (do (printf "Found %s genomes. Capping to %s.\n"
                  genome-count
                  population-size)
          (concat genomes (genome/create-initial-population
                           const/used-genome-keys
                           (- population-size genome-count))))
      (do (printf "Loaded %s genomes (needed %s).\n"
                  genome-count
                  population-size)
          genomes))))

(defn -main [& _args]
  (println "Version:" (get-git-revision))
  (let [deserialized (deserialize)
        generation (or (:generation deserialized) 0)
        max-generations (or (:max-generations deserialized) 500)
        population-size (or (:population-size deserialized) 150)
        genomes (ensure-genomes (:genomes deserialized) population-size)
        max-tetrominoes-count (or (:max-tetrominoes-count deserialized) 100000)
        calc-score-fn (genome/build-calculate-score-fn const/used-genome-keys
                                                       genome/genome-coeff-fns)]
    (ai-core/train
     calc-score-fn
     generation
     max-generations
     genomes
     max-tetrominoes-count
     population-size
     (partial serialize-fn max-generations population-size max-tetrominoes-count))))
