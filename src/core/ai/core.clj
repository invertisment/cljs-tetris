(ns core.ai.core
  (:require
   [core.actions.move :as move]
   [core.actions.piece-gen :as piece-gen]
   [core.ai.compute-utils :as compute]
   [core.ai.genome :as genome]
   [core.ai.moves :as moves]
   [core.ai.placement :as placement]
   [core.constants :as const]))

(def get-score
  (comp :lines-cleared :score))

(defn mk-n-states [field-count tetrominoes-count]
  (map
   (fn [_]
     (move/new-field
      (doall
       (repeatedly
        tetrominoes-count
        (partial piece-gen/generate-new-piece const/pieces)))))
   (range field-count)))
#_(mk-n-states 10 10)

(defn best-of-n [is-game-ended-fn pick-better-state-bifn calc-score-fn genome states]
  (let [results (compute/future-map
                 (fn [state]
                   (placement/apply-pieces-while
                    is-game-ended-fn
                    #_(partial placement/place-best-look2-piece is-game-ended-fn)
                    placement/place-best-look1-piece
                    pick-better-state-bifn
                    calc-score-fn
                    #_(partial placement/place-best-2deepcheap-piece is-game-ended-fn)
                    genome
                    state))
                 states)
        scores (map get-score results)]
    {:genome genome
     ;; Bias towards highest score of both games
     :final-score #_(reduce + scores)
     ;;;; Bias towards high-performing but a little risky genome
     ;;:final-score (+ (reduce + scores)
     ;;                (reduce max 0 scores))
     ;; Lowest score elimination
     ;;:final-score (reduce min (first scores) scores)
     ;;
     ;; minimum-stability and some awesome performance
     ;; log addition is like number multiplication but doesn't go into large numbers
     (+ (Math/log (reduce min (first scores) scores))
        (Math/log (reduce + 0 scores)))
     :scores scores
     :results results}))

(defn train-any [step-title is-game-ended-fn pick-better-state-bifn calc-score-fn field-count tetrominoes-count genomes]
  (let [states (mk-n-states field-count tetrominoes-count)
        elites-with-state (->> genomes
                               (compute/map-w-progress
                                step-title
                                (fn [genome]
                                  (best-of-n is-game-ended-fn pick-better-state-bifn calc-score-fn genome states)))
                               (sort-by :final-score)
                               (reverse))]
    (println "Best performances:" (mapv :scores (take 10 elites-with-state))
             "\nWorst performances:" (mapv :scores (take 10 (reverse elites-with-state)))
             "\nBest genome:" (:genome (first elites-with-state)))
    elites-with-state))
(comment (def outcome (doall (train-any
                              "gen1"
                              moves/is-game-ended?
                              placement/pick-better-score
                              (genome/build-calculate-score-fn const/used-genome-keys
                                                               genome/genome-coeff-fns)
                              1
                              1000
                              (genome/create-initial-population const/used-genome-keys 10)))))
(comment (map #(dissoc % :genome :results) outcome))

(defn generate-genomes-from-winners [gen-number population-size seed-population-size best-genomes]
  (let [elites (->> best-genomes
                    (take seed-population-size)
                    #_(genome/filter-distinct))]
    (->> (cycle elites)
         (map (partial genome/make-child
                       gen-number
                       elites))
         (concat elites)
         (take population-size))))
#_(generate-genomes-from-winners 1 30 3 [{:safe {:s 1} :risky {:r 1.1}}
                                         {:safe {:s 2} :risky {:r 2.1}}
                                         {:safe {:s 3} :risky {:r 3.1}}
                                         {:safe {:s 4} :risky {:r 4.1}}
                                         {:safe {:s 5} :risky {:r 5.1}}])

(defn train [calc-score-fn gen-number max-generations genomes tetrominoes-count population-size serialize-fn!]
  (println "Training" population-size "genomes for" max-generations "generations with" tetrominoes-count "pieces each"
           (format "from generation %s." gen-number))
  (loop [genomes genomes
         gen-number gen-number]
    (serialize-fn! gen-number genomes)
    (if (< gen-number max-generations)
      (let [field-count 2]
        (recur
         (generate-genomes-from-winners gen-number
                                        population-size
                                        3 #_(quot population-size 2)
                                        (->> (train-any
                                              (str "Generation " gen-number)
                                              moves/is-game-ended?
                                              placement/pick-better-score
                                              calc-score-fn
                                              field-count
                                              tetrominoes-count
                                              genomes)
                                             (map :genome)))
         (inc gen-number)))
      (do (println "Max generations reached. Exiting.")
          genomes))))
