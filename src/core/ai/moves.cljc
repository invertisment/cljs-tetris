(ns core.ai.moves
  (:require [core.actions.move :as move]
            [core.piece-validators :as v]))

(defn propagate-move [before-move-fn before-stick-fn  move-fn path-key move]
  (loop [{:keys [path state]} move
         all-iterations []]
    (let [moved (move-fn
                 v/field-valid?
                 identity
                 before-move-fn before-stick-fn
                 state)]
      (if (or (= (:piece moved) (:piece state))
              (nil? moved))
        all-iterations
        (let [next {:path (concat path [path-key])
                    :state moved}]
          (recur
           next
           (cons next all-iterations)))))))

(defn make-single-move [before-move-fn before-stick-fn move-fn path-key {:keys [path state]}]
  {:path (concat path [path-key])
   :state (move-fn
           v/field-valid?
           identity
           before-move-fn before-stick-fn
           state)})

(defn propagate-move-nonrepeatable [before-move-fn before-stick-fn move-fn path-key move]
  (make-single-move before-move-fn before-stick-fn move-fn path-key move))

(defn valid-move? [{:keys [state] :as move}]
  (v/field-valid? state))

(defn is-game-ended? [state]
  (= :ended (:game-state state)))

(defn propagate-move-iterate [move-fn before-move-fn before-stick-fn path-key move iterations]
  (filter
   valid-move?
   (loop [i iterations
          all-iterations [move]
          current-move move]
     (if (<= i 0)
       all-iterations
       (let [new-move (make-single-move move-fn before-move-fn before-stick-fn path-key current-move)]
         (if (is-game-ended? (:state new-move))
           (do
             #_(println "(= :ended (:game-state (:state new-move)))")
             all-iterations)
           (recur
            (dec i)
            (conj
             all-iterations
             new-move)
            new-move)))))))

(defn find-moves-left [before-move-fn before-stick-fn move]
  (propagate-move before-move-fn before-stick-fn move/left :left move))

(defn find-moves-right [before-move-fn before-stick-fn move]
  (propagate-move before-move-fn before-stick-fn move/right :right move))

(defn find-move-bottom [before-move-fn before-stick-fn move]
  (propagate-move-nonrepeatable before-move-fn before-stick-fn move/bottom :bottom move))

(defn find-move-down [before-move-fn before-stick-fn move]
  (propagate-move-nonrepeatable before-move-fn before-stick-fn move/down :bottom move))

(defn find-moves-bottom [before-move-fn before-stick-fn move]
  [(find-move-bottom before-move-fn before-stick-fn move)])

(defn find-moves-rotate [before-move-fn before-stick-fn move]
  (propagate-move-iterate before-move-fn before-stick-fn move/rotate :rotate move 3))

(defn find-piece-placements [before-move-fn before-stick-fn move]
  #_(println "find-piece-placements count" (count (:next-pieces (:state move))))
  (let [rotated-moves (find-moves-rotate before-move-fn before-stick-fn move)]
    (mapcat
     (partial find-moves-bottom
              before-move-fn
              before-stick-fn)
     (concat (mapcat (partial find-moves-left
                              before-move-fn
                              before-stick-fn)
                     rotated-moves)
             rotated-moves
             (reverse (mapcat (partial find-moves-right
                                       before-move-fn
                                       before-stick-fn)
                              (reverse rotated-moves)))))))
