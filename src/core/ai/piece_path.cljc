(ns core.ai.piece-path)

(defn before-stick-piece! [{:keys [piece piece-path] :as stateT}]
  (-> stateT
      (dissoc! :piece-path)
      (assoc! :prev-piece-path (into piece-path piece))))

(defn before-stick-piece-noop [stateT] stateT)

(defn before-move! [{:keys [piece piece-path] :as fieldT}]
  (assoc! fieldT :piece-path (into piece-path piece)))

(defn before-move-noop [fieldT] fieldT)
