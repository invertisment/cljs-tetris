(ns core.ai.genome
  (:require
   [core.ai.dag-exec :as dag-exec]
   [core.ai.move-analysis :as move-analysis]
   [plumbing.core :refer [fnk]]))

(defn abs [n] (max n (- n)))

(def mutation-rate 0.2)
(def mutation-step 0.05)

(defn new-initial-coefficient []
  (- (rand) 0.5))

(defn gen-genome-name []
  (str "genome-" (rand)))

(def max-clearable-lines 4)

#_{:clj-kondo/ignore [:unresolved-symbol]}
(def genome-coeff-fns
  {;; rows cleared per piece placement
   :rows-cleared [[:state] (fnk [state]
                                (get-in state [:score :lines-cleared] 0))]
   :heights-from-bottom [[:state] (fnk [state]
                                       (move-analysis/find-heights-from-bottom state))]
   :relative-heights [[:heights-from-bottom] (fnk [heights-from-bottom]
                                                  (move-analysis/find-relative-heights heights-from-bottom))]
   :max-piece-height [[:relative-heights] (fnk [relative-heights]
                                               (move-analysis/height relative-heights))]
   ;; absolute height of the highest column to the power of 2
   :weighted-height [[:max-piece-height] (fnk [max-piece-height]
                                              max-piece-height)]
   ;; sum of all block heights
   :cumulative-height [[:heights-from-bottom] (fnk [heights-from-bottom]
                                                   (move-analysis/cumulative-height heights-from-bottom))]
   ;; sum of all empty cells underground
   :hole-coords [[:state] (fnk [state] (move-analysis/find-hole-coords state))]
   :hole-count [[:hole-coords] (fnk [hole-coords]
                                    (move-analysis/count-holes hole-coords))]
   ;; sum of absolute differences between neighbours
   ;; if the game field is empty this is zero
   ;; except one outlier (for deepest well)
   :roughness-and-flatness [[:heights-from-bottom] (fnk [heights-from-bottom]
                                                        (move-analysis/field-roughness-flatness heights-from-bottom))]
   :roughness [[:roughness-and-flatness] (fnk [roughness-and-flatness]
                                              (let [[roughness _flatness] roughness-and-flatness]
                                                roughness))]
   ;; sum of all flat neighbours
   ;; if the game field is empty this is the width of the map
   ;; except one outlier (for deepest well)
   :flatness [[:roughness-and-flatness] (fnk [roughness-and-flatness]
                                             (let [[_roughness flatness] roughness-and-flatness]
                                               flatness))]
   ;; deepest side hole near side of the map
   :well-depth-at-wall [[:heights-from-bottom] (fnk [heights-from-bottom]
                                                    (move-analysis/well-depth-at-wall heights-from-bottom))]
   ;;;; deepest side hole 1px from sides
   ;;:well-depth-one-px-from-wall
   ;; sum of hole depths subtracted from field height (consecutive too)
   :well-depth-at-wall-minus-4 [[:well-depth-at-wall] (fnk [well-depth-at-wall]
                                                           (max (- well-depth-at-wall max-clearable-lines) 0))]
   ;;;; deepest side hole 1px from sides
   ;;:well-depth-one-px-from-wall-minus-4
   ;;;;sum of hole depths subtracted from field height (consecutive too)
   ;;:reverse-field-hole-depth-sum
   ;;;; measure how full the lines are (^2 is needed to counteract placement anywhere)
   :horizontal-fullness [[:state] (fnk [state]
                                       (move-analysis/count-horizontal-space state))]
   ;; Relative steps between two adjacent pixels
   :grouped-stepcounts-0-2 [[:heights-from-bottom]
                            (fnk [heights-from-bottom]
                                 (move-analysis/count-grouped-step-counts (move-analysis/count-steps heights-from-bottom)
                                                                          :step-more
                                                                          :step-0
                                                                          :step-1
                                                                          :step-2
                                                                          #_:step-3
                                                                          #_:step-4
                                                                          #_:step-5))]
   :step-0 [[:grouped-stepcounts-0-2] (fnk [grouped-stepcounts-0-2] (or (:step-0 grouped-stepcounts-0-2) 0))]
   :step-1 [[:grouped-stepcounts-0-2] (fnk [grouped-stepcounts-0-2] (or (:step-1 grouped-stepcounts-0-2) 0))]
   :step-2 [[:grouped-stepcounts-0-2] (fnk [grouped-stepcounts-0-2] (or (:step-2 grouped-stepcounts-0-2) 0))]
   ;;:step-3 [[:grouped-stepcounts] (fnk [grouped-stepcounts] (or (:step-3 grouped-stepcounts) 0))]
   ;;:step-4 [[:grouped-stepcounts] (fnk [grouped-stepcounts] (or (:step-4 grouped-stepcounts) 0))]
   ;;:step-5 [[:grouped-stepcounts] (fnk [grouped-stepcounts] (or (:step-5 grouped-stepcounts) 0))]
   :step-more [[:grouped-stepcounts-0-2] (fnk [grouped-stepcounts-0-2] (or (:step-more grouped-stepcounts-0-2) 0))]
   ;; fancy hole stuff
   :hole-setback [[:state :hole-coords] (fnk [state hole-coords]
                                             (move-analysis/count-hole-setback state hole-coords))]
   ;; how many lines can be cleared
   :min-piece-height [[:state :relative-heights]
                      (fnk [state relative-heights]
                           (move-analysis/min-height state relative-heights))]
   :clearable-line-count [[:state :max-piece-height :min-piece-height]
                          (fnk [state max-piece-height min-piece-height]
                               (move-analysis/find-clearable-line-count state max-piece-height min-piece-height))]})

(defn new-initial-genome-var-map [genome-keys]
  (->> genome-keys
       (map (juxt identity (fn [& _] (new-initial-coefficient))))
       (into {})))

;; https://www.youtube.com/watch?v=xLHCMMGuN0Q
(defn new-initial-genome [genome-keys]
  (merge
   {:id (gen-genome-name)}
   {:safe (new-initial-genome-var-map genome-keys)
    :risky (new-initial-genome-var-map genome-keys)}))
#_(new-initial-genome)

;; What didn't work:
;; hole depth of first three lines: it tries to produce holes in the beginning of the game
;; horizontal fullness (sum of (line px count that are ^2)):
;;   positive value: It tries to produce holes to retain the score
;;   negative value: It tries to build up

(defn create-initial-population [genome-keys population-size]
  (repeatedly population-size (fn [] (new-initial-genome genome-keys))))

(defn is-many-pixels [state]
  (let [pixel-count (move-analysis/count-pixels state)]
    (> pixel-count 46)))

(defn pick-subgenome [state]
  (if (is-many-pixels state)
    :safe ;; typo
    :risky))

(defn build-calculate-score-fn [used-genome-keys coeff-fn-map]
  (let [compiled-fn (dag-exec/build used-genome-keys coeff-fn-map)]
    (fn [full-genome {:keys [state] :as _move}]
      (let [genome ((pick-subgenome state) full-genome)
            result (compiled-fn {:state state})]
        (->> used-genome-keys
             (reduce (fn [out genome-key]
                       #_(assert (genome-key genome) (str genome-key " should is null"))
                       (+ out
                          (* (genome-key genome)
                             (genome-key result))))
                     0))))))

(defn crossover [mom-genome dad-genome]
  (reduce
   (fn [child-genome k]
     (assoc child-genome
            k (rand-nth [(k mom-genome) (k dad-genome)])))
   {}
   (keys mom-genome)))

(defn adjust-mutation-param [generation parameter]
  parameter
  ;; uses hyperbola to adjust by generation:
  #_(/ (* 100 parameter) (+ 80 generation)))

(defn mutate-param [generation param]
  (+ param (* (- (rand) 0.5)
              (adjust-mutation-param generation mutation-step))))

(defn mutate [gen-number genome]
  #_(reduce
     (fn [genome k]
       (if (and (> mutation-rate (rand)) (number? (k genome)))
         (update genome k mutate-param)
         genome))
     genome
     (keys genome))
  (let [adjusted-mutation-rate (adjust-mutation-param gen-number mutation-rate)
        mutated-genome-kv-list (map
                                (fn [[k orig-v]]
                                  [k (if (> adjusted-mutation-rate (rand))
                                       (mutate-param gen-number orig-v)
                                       orig-v)])
                                genome)
        max-value (->> mutated-genome-kv-list
                       (map (comp abs second))
                       (reduce max 1))]
    (into
     {}
     (if (> max-value 1)
       (map
        (fn [[k orig-v]]
          [k (/ orig-v max-value)])
        mutated-genome-kv-list)
       mutated-genome-kv-list))))
#_(mutate 0 {:hi 50 :hi1 1})
#_(mutate 0 {:hi 0.50 :hi1 1.02})
#_(mutate 0 {:hi 0.50 :hi1 -1.02})

(defn make-child-nested [gen-number mom-genome dad-genome & subgenome-keys]
  (reduce (fn [out-genome k]
            (assoc out-genome
                   k (mutate gen-number (crossover (k mom-genome)
                                                   (k dad-genome)))))
          (assoc mom-genome
                 :id (gen-genome-name))
          subgenome-keys))
#_(let [a (new-initial-genome)
        b (new-initial-genome)]
    [a (make-child-nested a b :risky)])

(defn make-child [gen-number elites mom-genome]
  (make-child-nested gen-number mom-genome (rand-nth elites) :risky :safe))

;; make sure every weight key exists in genome
(defn ensure-weight-existence-single [genome-var-map genome-keys]
  (merge (->> genome-keys
              (map (juxt identity (constantly 0)))
              (into {}))
         genome-var-map))

;; make sure every weight key exists in genome
(defn ensure-weight-existence [genome-keys genome]
  (merge
   genome
   {:id (gen-genome-name)}
   (-> genome
       (update :risky ensure-weight-existence-single genome-keys)
       (update :safe ensure-weight-existence-single genome-keys))))
#_(ensure-weight-existence [:hi :there] {})

(defn genome-vals-similarity [genome-a-vals genome-b-vals]
  (->> (keys genome-a-vals)
       (map (fn [k] (abs (- (genome-a-vals k 0)
                            (genome-b-vals k 0)))))
       (reduce +)))

(defn similar? [genome-a threshold genome-b]
  (> threshold
     (+ (genome-vals-similarity (:safe genome-a) (:safe genome-b))
        (genome-vals-similarity (:risky genome-a) (:risky genome-b)))))
#_(similar? {:safe {:a 123 :b 1}} 1 {:safe {:a 124}})
#_(similar? {:risky {:a 123 :b 1}} 1 {:risky {:a 124}})
#_(similar? {:safe {:a 124}} 1 {:safe {:a 123 :b 1}})
#_(similar? {:safe {:a 123 :b 1}} 2 {:safe {:a 124}})

(defn filter-distinct-by-threshold [threshold best-sorted-genomes]
  (->> best-sorted-genomes
       (reduce
        (fn [out genome]
          (if (some
               (partial similar? genome threshold)
               out)
            out
            (cons genome out)))
        [])
       reverse))
#_(filter-distinct-by-threshold 1 [{:a 123 :b 1} {:a 124}])
#_(filter-distinct-by-threshold 1.001 [{:a 123 :b 1} {:a 124}])
#_(filter-distinct-by-threshold 0.5 [{:a 123 :b 1} {:a 124}])

(defn filter-distinct [best-sorted-genomes]
  (println "filter-distinct 1")
  (let [filtered (filter-distinct-by-threshold (/ mutation-step 10) best-sorted-genomes)]
    (println "filter-distinct 2")
    (println "Removed" (- (count best-sorted-genomes) (count filtered)) "of similar genomes.")
    filtered))
