(ns core.ai.dag-exec
  (:require
   [loom.alg :as alg]
   [loom.graph :as g]
   [plumbing.graph :as pgraph]
   [clojure.set :as sets]))

(defn get-deps [m]
  (->> m
       (mapcat (fn [[k [deps _f]]]
                 (->> deps
                      (mapv (fn [item]
                              [k item])))))))

(defn find-eval-order [nodes m]
  (let [g (apply g/digraph (get-deps m))]
    (->> nodes
         (reduce (fn [found node]
                   (if (found node)
                     found
                     (if-let [path (alg/topsort g node)]
                       (sets/union found (set path))
                       (throw (ex-info "Can't produce an eval order." {:data m})))))
                 #{}))))

(defn filter-coeff-fns [result-coeff-kws genome-coeff-fn-map]
  (->> (find-eval-order result-coeff-kws genome-coeff-fn-map)
       (select-keys genome-coeff-fn-map)))

(defn to-pgraph-struct [genome-coeff-fn-map]
  (->> genome-coeff-fn-map
       (mapv (fn [[k v]]
               [k (second v)]))
       (into {})))

(defn build [result-coeff-kws genome-coeff-fn-map]
  (as-> genome-coeff-fn-map %
    (filter-coeff-fns result-coeff-kws %)
    (to-pgraph-struct %)
    (pgraph/compile %)))
