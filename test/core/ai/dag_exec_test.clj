(ns core.ai.dag-exec-test
  (:require
   [clojure.test :refer [deftest is testing]]
   [core.ai.dag-exec :as dag-exec]
   [plumbing.core :refer [fnk]]))

(deftest get-deps-test
  (testing "should find dependencies between computations"
    (is (= [[:sum :a] [:sum :b]]
           (dag-exec/get-deps
            {:sum [[:a :b] (fn [a b]
                             (+ a b))]
             :a [[] (fn [] 15)]
             :b [[] (fn [] 20)]})))))

(deftest find-eval-order-test
  (testing "should construct dag graph"
    (is (= #{:b :a :sum}
           (dag-exec/find-eval-order
            [:sum]
            {:sum [[:a :b] (fn [a b]
                             (+ a b))]
             :a [[] (fn [] 15)]
             :b [[] (fn [] 20)]}))))
  (testing "should not include unused items"
    (is (= #{:b :a :sum}
           (dag-exec/find-eval-order
            [:sum]
            {:sum [[:a :b] (fn [a b]
                             (+ a b))]
             :a [[] (fn [] 15)]
             :unused [[:a] (fn [a] a)]
             :b [[] (fn [] 20)]}))))
  (testing "should not create order when there's cycle"
    (is (thrown-with-msg?
         clojure.lang.ExceptionInfo
         #"Can't produce an eval order\."
         (dag-exec/find-eval-order
          [:sum]
          {:sum [[:a :b] (fn [a b]
                           (+ a b))]
           :a [[:sum] (fn [] 15)]
           :unused [[:a] (fn [a] a)]
           :b [[] (fn [] 20)]})))))

(def sample-unusable-genome-coeff-fns
  {:sum [[:a :b] 'code_sum]
   :a [[] 'code_a]
   :b [[] '(fnk ...)]})

(deftest filter-coeffs-test
  (testing "should only retain coefficients that are used by something [1]"
    (is (= {:sum 'code_sum
            :a 'code_a
            :b '(fnk ...)}
           (dag-exec/to-pgraph-struct (dag-exec/filter-coeff-fns
                                       [:sum]
                                       sample-unusable-genome-coeff-fns)))))
  (testing "should only retain coefficients that are used by something [2]"
    (is (= {:b '(fnk ...)
            :a 'code_a}
           (dag-exec/to-pgraph-struct (dag-exec/filter-coeff-fns
                                       [:b :a]
                                       sample-unusable-genome-coeff-fns))))))

(def sample-usable-genome-coeff-fns
  {:sum [[:a :b :user-input] (fnk [a b user-input] (+ a b user-input))]
   :a [[] (fnk [] 12)]
   :b [[] (fnk [] 25)]})

(deftest build-test
  (testing "should build optimized fn"
    (is (= {:b 25
            :a 12
            :sum 3037}
           (into {} ((dag-exec/build
                      [:sum]
                      sample-usable-genome-coeff-fns)
                     {:user-input 3000}))))))
