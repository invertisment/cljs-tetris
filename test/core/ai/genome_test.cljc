(ns core.ai.genome-test
  (:require
   [clojure.test :refer :all]
   [core.actions.move :as move]
   [core.ai.genome :as genome]
   [core.ai.util :as util]
   [core.ai.piece-path :as piece-path]))

(deftest ensure-weight-existence-test
  (testing "smoke check"
    (is (= #{:id :safe :risky}
           (set (keys (genome/ensure-weight-existence [:hi :there] {}))))))
  (testing "should add zero coeffs"
    (is (= #{0}
           (->> (dissoc (genome/ensure-weight-existence [:hi :there] {}) :id)
                vals
                (mapcat vals)
                (into #{})))))
  (testing "should persist vals"
    (is (= #{0 "not changed"}
           (->> (dissoc (genome/ensure-weight-existence
                         [:well-depth-at-wall-minus-4]
                         {:id "nani"
                          :risky {:well-depth-at-wall-minus-4 "not changed"}})
                        :id)
                vals
                (mapcat vals)
                (into #{}))))))

(def unfinished-bridge-field
  (util/new-field
   piece-path/before-move!
   piece-path/before-stick-piece!
   [util/z-piece
    util/line-piece
    util/line-piece
    util/square-piece]
   [move/left move/left move/left move/bottom
    move/left move/bottom
    move/right move/right move/bottom]))

(def sample-genome-keys
  [:rows-cleared
   :weighted-height
   :cumulative-height
   :hole-count
   :roughness
   :flatness
   :well-depth-at-wall
   :well-depth-at-wall-minus-4
   :horizontal-fullness
   :step-0
   :step-1
   :step-2
   :step-more
   :hole-setback
   :clearable-line-count])

(def unit-genome
  (->> sample-genome-keys
       (map (juxt identity (constantly 1)))
       (into {})
       ((fn [g]
          {:safe g
           :risky g}))))

(deftest build-calculate-score-fn-test
  (testing "should return fn that calculates score"
    (is (number?
         (let [f (genome/build-calculate-score-fn sample-genome-keys
                                                  genome/genome-coeff-fns)]
           (f unit-genome {:state unfinished-bridge-field})))))
  (testing "should allow to build custom fn"
    (is (number?
         (let [f (genome/build-calculate-score-fn [:step-0 :step-1 :step-2 :step-more]
                                                  genome/genome-coeff-fns)]
           (f unit-genome {:state unfinished-bridge-field}))))))
