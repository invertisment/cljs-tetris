(ns core.ai.placement-test
  (:require [core.ai.placement :as placement]
            [clojure.test :refer :all]
            [core.ai.util :as util]
            [core.ai.genome :as genome]
            [core.ai.moves :as moves]
            [core.actions.move :as move]
            [core.ai.genome-test :as genome-test]
            [core.ai.piece-path :as piece-path]))

(def clearable-line-in-two-moves-field
  (util/new-field
   piece-path/before-move!
   piece-path/before-stick-piece!
   [util/line-piece
    util/line-piece
    util/t-piece
    util/line-piece]
   [move/left move/left move/left move/bottom
    move/right move/bottom
    ;; These are the clearing moves:
    ;;move/rotate-counter-clockwise move/right move/right move/right move/right move/bottom
    ;;move/rotate-counter-clockwise move/right move/right move/right move/right move/right move/bottom
    ]))

(def clearable-2-lines-using-next-piece-field
  (util/new-field
   piece-path/before-move!
   piece-path/before-stick-piece!
   [util/line-piece
    util/line-piece
    util/line-piece
    util/line-piece
    util/t-piece ;; current piece
    util/square-piece ;; next piece
    util/square-piece ;; next-next piece
    util/square-piece]
   [move/left move/left move/left move/bottom
    move/left move/left move/left move/bottom
    move/right move/bottom
    move/right move/bottom]))

(def clearable-2-lines-using-first-piece-field
  (util/new-field
   piece-path/before-move!
   piece-path/before-stick-piece!
   [util/line-piece
    util/line-piece
    util/line-piece
    util/line-piece
    util/square-piece ;; current piece
    util/t-piece ;; next piece
    util/square-piece ;; next-next piece
    util/square-piece]
   [move/left move/left move/left move/bottom
    move/left move/left move/left move/bottom
    move/right move/bottom
    move/right move/bottom]))

(def noop-genome (genome/ensure-weight-existence genome-test/sample-genome-keys {}))
(def line-clear-genome (-> (genome/ensure-weight-existence genome-test/sample-genome-keys {})
                           (update-in [:safe :rows-cleared] inc)
                           (update-in [:risky :rows-cleared] inc)))

(deftest find-ranked-piece-placements-test
  (testing "should find 34 moves; genome coeffs are zero"
    (is (= {0 34}
           (->> (placement/find-ranked-piece-placements
                 identity
                 identity
                 (genome/build-calculate-score-fn genome-test/sample-genome-keys
                                                  genome/genome-coeff-fns)
                 noop-genome
                 (placement/to-placement clearable-line-in-two-moves-field))
                (map first)
                frequencies)))))

(deftest pick-best-1deep-piece-placement-test
  (testing "should take first when undecidable square"
    (is (= [:left :left :left :left :bottom]
           (:path (placement/internal-pick-best-1deep-piece-placement
                   placement/pick-better-score
                   identity
                   identity
                   (genome/build-calculate-score-fn genome-test/sample-genome-keys
                                                    genome/genome-coeff-fns)
                   noop-genome
                   (placement/to-placement
                    (util/new-field
                     identity
                     identity
                     [util/square-piece]
                     [])))))))
  (testing "should take first when undecidable line"
    (is (= [:left :left :left :bottom]
           (:path (placement/internal-pick-best-1deep-piece-placement
                   placement/pick-better-score
                   identity
                   identity
                   (genome/build-calculate-score-fn genome-test/sample-genome-keys
                                                    genome/genome-coeff-fns)
                   noop-genome
                   (placement/to-placement
                    (merge
                     util/empty-field
                     util/line-piece)))))))
  (testing "should take first when undecidable (new field created by move package)"
    (is (= [:left :left :left :left :bottom]
           (:path (placement/internal-pick-best-1deep-piece-placement
                   placement/pick-better-score
                   identity
                   identity
                   (genome/build-calculate-score-fn genome-test/sample-genome-keys
                                                    genome/genome-coeff-fns)
                   noop-genome
                   (placement/to-placement
                    (move/new-field
                     (take 3 (repeat util/square-piece))))))))))

(deftest pick-best-2deep-piece-placement-test
  (testing "should find that it's possible to clear a line (without :lines-cleared coeff)"
    (is (= [:rotate :rotate :rotate :right :right :right :right :bottom]
           (:path (placement/pick-best-2deep-piece-placement
                   (constantly false)
                   placement/pick-better-score
                   (genome/build-calculate-score-fn genome-test/sample-genome-keys
                                                    genome/genome-coeff-fns)
                   line-clear-genome
                   clearable-line-in-two-moves-field)))))
  (testing "should return regular next piece if game is ended"
    (is (= [:left :left :left :bottom]
           (:path (placement/pick-best-2deep-piece-placement
                   (constantly true)
                   placement/pick-better-score
                   (genome/build-calculate-score-fn genome-test/sample-genome-keys
                                                    genome/genome-coeff-fns)
                   line-clear-genome
                   clearable-line-in-two-moves-field))))))

(deftest swap-next-piece-test
  (testing "should find that it's possible to clear two lines instead of one using next piece"
    (is (= [[{:coord [5 1] :color "g"}
             {:coord [4 1] :color "g"}
             {:coord [5 0] :color "g"}
             {:coord [4 0] :color "g"}]
            {:piece
             [{:coord [4 1] :color "r"}
              {:coord [5 1] :color "r"}
              {:coord [4 0] :color "r"}
              {:coord [3 1] :color "r"}]
             :piece-bounds {:x-range [3 6] :y-range [0 3]}
             :color "rebeccapurple"}]
           ((juxt
             :piece
             (comp first :next-pieces)) (placement/swap-next-piece
                                         (constantly false)
                                         clearable-2-lines-using-next-piece-field))))))

(deftest pick-best-2deepcheap-piece-placement-test
  (testing "should find that it's possible to clear two lines when planning next piece first"
    (is (= [:left :left :left :bottom]
           (:path (placement/pick-best-2deepcheap-piece-placement
                   (constantly false)
                   placement/pick-better-score
                   (genome/build-calculate-score-fn genome-test/sample-genome-keys
                                                    genome/genome-coeff-fns)
                   line-clear-genome
                   clearable-2-lines-using-next-piece-field)))))
  (testing "should plan first two moves (and give :path for the first move only)"
    (is (= [:right :right :right :right :bottom]
           (:path (placement/pick-best-2deepcheap-piece-placement
                   (constantly false)
                   placement/pick-better-score
                   (genome/build-calculate-score-fn genome-test/sample-genome-keys
                                                    genome/genome-coeff-fns)
                   line-clear-genome
                   clearable-2-lines-using-first-piece-field))))))

(defn is-game-ended? [state]
  (= :ended (:game-state state)))

(deftest apply-pieces-test
  (testing "should apply moves"
    (is (= {nil 208, "cyan" 12}
           (->> (placement/apply-pieces-while
                 is-game-ended?
                 placement/place-best-look1-piece
                 placement/pick-better-score
                 (genome/build-calculate-score-fn genome-test/sample-genome-keys
                                                  genome/genome-coeff-fns)
                 (genome/ensure-weight-existence genome-test/sample-genome-keys {})
                 (assoc
                  (merge
                   util/empty-field
                   util/line-piece)
                  :next-pieces
                  (take 2 (repeat util/line-piece))))
                (:field)
                (reduce concat)
                (frequencies)))))
  (testing "should clear 2 lines with 10 squares as input"
    (is (= {:lines-cleared 2}
           (:score
            (placement/apply-pieces-while
             is-game-ended?
             placement/place-best-look1-piece
             placement/pick-better-score
             (genome/build-calculate-score-fn genome-test/sample-genome-keys
                                              genome/genome-coeff-fns)
             (assoc-in (genome/ensure-weight-existence genome-test/sample-genome-keys {})
                       [:risky :weighted-height] -1)
             (move/new-field (take 5 (repeat util/square-piece))))))))
  (testing "should be able to apply every move strategy fn"
    (is (= [true true true]
           (->> [placement/place-best-look1-piece
                 placement/place-best-look2-piece
                 placement/place-best-2deepcheap-piece]
                (map
                 (fn [f]
                   (boolean (placement/apply-pieces-while
                             is-game-ended?
                             f
                             placement/pick-better-score
                             (genome/build-calculate-score-fn genome-test/sample-genome-keys
                                                              genome/genome-coeff-fns)
                             (genome/ensure-weight-existence genome-test/sample-genome-keys {})
                             (move/new-field (take 5 (repeat util/square-piece)))))))
                doall)))))
