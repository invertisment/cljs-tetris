(ns core.actions.move-test
  (:require
   [clojure.test :refer :all]
   [core.actions.move :as move]
   [core.ai.piece-path :as piece-path]
   [core.ai.util :as util]
   [core.constants :as const]
   [core.piece-validators :as validators]))

(defmacro count-calls [fn-name body]
  `(let [counter# (atom 0)
         ~fn-name (fn [_] (swap! counter# inc))]
     ~body
     @counter#))

(defn every=? [value items]
  (every? #(= value %) items))

(deftest determine-direction-test
  (testing "should have left"
    (is (every=? #'core.actions.move/left (map move/direction const/left))))
  (testing "should have right"
    (is (every=? #'core.actions.move/right (map move/direction const/right))))
  (testing "should have rotate"
    (is (every=? #'core.actions.move/rotate (map move/direction const/rotate-clockwise))))
  (testing "should have down"
    (is (every=? #'core.actions.move/down (map move/direction const/down))))
  (testing "should have bottom"
    (is (every=? #'core.actions.move/bottom (map move/direction const/bottom))))
  (testing "should have default case"
    (is (= #'core.actions.move/nop (move/direction "anything else")))))

(def sample-field
  (util/new-field
   piece-path/before-move!
   piece-path/before-stick-piece!
   [util/line-piece
    util/line-piece
    util/z-piece
    util/line-piece
    util/z-piece
    util/z-piece
    util/z-piece
    util/z-piece
    util/z-piece
    util/line-piece
    util/line-piece
    util/line-piece
    util/line-piece
    util/line-piece
    util/line-piece
    util/line-piece
    util/line-piece
    util/line-piece
    util/line-piece]
   []))

(deftest bottom-test
  (testing "should not do infinite loop on no :piece"
    (is (= nil (move/bottom (constantly true) identity identity identity {}))))
  (testing "should put piece to bottom of the field"
    (is (= [nil nil nil "c" "c" "c" "c" nil nil nil]
           (->> sample-field
                (move/bottom validators/field-valid? identity identity identity)
                :field
                last))))
  (testing "should all pieces to bottom of the field"
    (is (= [[nil nil nil nil nil nil nil nil nil nil]
            [nil nil nil "c" "c" "c" "c" nil nil nil]
            [nil nil nil "o" "o" nil nil nil nil nil]
            [nil nil nil nil "o" "o" nil nil nil nil]
            [nil nil nil "c" "c" "c" "c" nil nil nil]
            [nil nil nil "c" "c" "c" "c" nil nil nil]]
           (->> sample-field
                (move/bottom validators/field-valid? identity identity identity)
                (move/bottom validators/field-valid? identity identity identity)
                (move/bottom validators/field-valid? identity identity identity)
                (move/bottom validators/field-valid? identity identity identity)
                :field
                (take-last 6)))))
  (testing "should drop all pieces"
    (is (= [[nil nil nil nil nil nil nil nil nil nil]
            [nil nil nil "c" "c" "c" "c" nil nil nil]
            [nil nil nil "c" "c" "c" "c" nil nil nil]
            [nil nil nil "c" "c" "c" "c" nil nil nil]
            [nil nil nil "c" "c" "c" "c" nil nil nil]
            [nil nil nil "c" "c" "c" "c" nil nil nil]
            [nil nil nil "c" "c" "c" "c" nil nil nil]
            [nil nil nil "o" "o" nil nil nil nil nil]
            [nil nil nil nil "o" "o" nil nil nil nil]
            [nil nil nil "o" "o" nil nil nil nil nil]
            [nil nil nil nil "o" "o" nil nil nil nil]
            [nil nil nil "o" "o" nil nil nil nil nil]
            [nil nil nil nil "o" "o" nil nil nil nil]
            [nil nil nil "o" "o" nil nil nil nil nil]
            [nil nil nil nil "o" "o" nil nil nil nil]
            [nil nil nil "o" "o" nil nil nil nil nil]
            [nil nil nil nil "o" "o" nil nil nil nil]
            [nil nil nil "c" "c" "c" "c" nil nil nil]
            [nil nil nil "o" "o" nil nil nil nil nil]
            [nil nil nil nil "o" "o" nil nil nil nil]
            [nil nil nil "c" "c" "c" "c" nil nil nil]
            [nil nil nil "c" "c" "c" "c" nil nil nil]]
           (->> (reduce
                 (fn [field _]
                   (move/bottom validators/field-valid? identity identity identity field))
                 sample-field
                 (range 15))
                :field))))
  (testing "should drop all pieces and end the game"
    (is (nil? (reduce
               (fn [field _]
                 (move/bottom validators/field-valid? identity identity identity field))
               sample-field
               (range 16))))))

(defn limit-next-pieces [state n]
  (update
   state
   :next-pieces
   (fn [infinite-seq]
     (take n infinite-seq))))

(defn remove-random-elements [state]
  (dissoc
   state
   :next-pieces
   :color
   :piece
   :piece-bounds))

(deftest new-game-test
  (testing "should create :piece"
    (is (= (sort [:color
                  :field
                  :game-state
                  :height
                  :levels
                  :next-pieces
                  :piece
                  :piece-bounds
                  :score
                  :width])
           (sort (keys (limit-next-pieces
                        (move/new-game
                         (constantly true)
                         identity
                         identity
                         identity
                         {})
                        1))))))
  (testing "should generate two distinct pieces"
    (is (not (let [{:keys [piece next-pieces]}
                   #_{:clj-kondo/ignore [:unresolved-namespace]}
                   (with-redefs
                    [core.actions.piece-gen/generate-new-piece
                     (fn [_]
                       {:piece (gensym "unique_for_testing_")
                        :piece-bounds (gensym "unique_for_testing_")})]
                     (move/new-game
                      (constantly true)
                      identity
                      identity
                      identity
                      {}))]
               (= piece (:piece (first next-pieces)))))))
  (testing "should generate two distinct piece bounds"
    (is (not
         (let [{:keys [piece-bounds next-pieces]}
               #_{:clj-kondo/ignore [:unresolved-namespace]}
               (with-redefs
                [core.actions.piece-gen/generate-new-piece
                 (fn [_]
                   {:piece (gensym "unique_for_testing_")
                    :piece-bounds (gensym "unique_for_testing_")})]
                 (move/new-game
                  (constantly true)
                  identity
                  identity
                  identity
                  {}))]
           (= piece-bounds (:piece-bounds (first next-pieces)))))))
  (testing "should have many nils in :field"
    (is (= {nil 220}
           (frequencies
            (reduce
             concat
             (:field (limit-next-pieces
                      (move/new-game
                       (constantly true)
                       identity
                       identity
                       identity
                       {})
                      1))))))))
