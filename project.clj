(defproject node_test "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [org.clojure/clojurescript "1.10.597"] ; 1.9.542 does not work
                 #_[org.clojure/clojurescript "1.11.121"] ;; bad performance
                 [org.clojure/core.async "1.1.582"]
                 [flames "0.4.0"]
                 [net.cgrand/xforms "0.19.2"]
                 [me.tongfei/progressbar "0.9.4"]
                 [prismatic/plumbing "0.6.0"]
                 [aysylu/loom "1.0.2"]]
  :profiles {:dev
             {:dependencies [[javax.xml.bind/jaxb-api "2.4.0-b180830.0359"] ;; workaround for java 8+
                             [com.clojure-goes-fast/clj-async-profiler "1.1.1"]
                             [criterium "0.4.6"]
                             [etaoin/etaoin "1.0.40"]]
              :plugins [[lein-cljsbuild "1.1.7"]]
              :source-paths ["src" "dev"]
              ;; https://github.com/clojure-goes-fast/clj-async-profiler
              :jvm-opts ["-Djdk.attach.allowAttachSelf"]}}
  :clean-targets
  [[:cljsbuild :builds 0 :compiler :output-to]
   :target-path
   :compile-path]
  :cljsbuild {:builds
              [{:id "dev"
                :source-paths ["src"]
                :compiler {:output-dir "out"
                           :output-to "index-dev.js"
                           :optimizations :none
                           :source-map true}}
               {:id "prod"
                :source-paths ["src"]
                :compiler {:output-to "index-prod.js"
                           :optimizations :whitespace}}]}
  :main core.ai.main)
