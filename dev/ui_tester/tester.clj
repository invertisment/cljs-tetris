(ns ui-tester.tester
  (:require [etaoin.api :as e]))

(defn start-driver! []
  (e/chrome {}))

(defn quit! [driver]
  (e/quit driver))

(defn navigate! [driver url]
  (e/go driver url)
  (e/wait driver 1))

(defn get-indexfile-loc []
  (str "file://" (System/getProperty "user.dir") "/index-dev.html"))

(defn reset-driver! [driver]
  (navigate! driver (get-indexfile-loc)))

(defn setup-driver! []
  (doto (start-driver!)
    reset-driver!))

(comment (def driver (setup-driver!)))
(comment (quit! driver))

(defn start-ai-game! [driver]
  (let [action (-> (e/make-key-input)
                   (e/add-key-press "m")
                   (e/add-pause 100)
                   (e/add-key-press "-")
                   (e/add-pause 100)
                   (e/add-key-press "n")
                   (e/add-pause 100))]
    (e/perform-actions driver action)))
(comment (start-ai-game! driver))

(defn parse-int [s]
  (Integer/parseInt s))

(defn get-current-score! [driver]
  (->> (e/js-execute driver "return document.querySelector('#lines-cleared').innerText")
       parse-int))
(comment (get-current-score! driver))

(defn hard-start-game! [driver]
  (doto driver
    reset-driver!
    start-ai-game!))

(defn single-pass! [driver time-s]
  (hard-start-game! driver)
  (Thread/sleep (* 1000 time-s))
  (println (format "Score after %ss: %s"
                   time-s
                   (get-current-score! driver))))
(comment (single-pass! driver 30))

(defn many-passes! [driver each-pass-time-s passes]
  (doseq [_i (range passes)]
    (single-pass! driver each-pass-time-s)))
(comment (many-passes! driver 60 10))
(comment (many-passes! driver 60 2))

(comment (def driver (setup-driver!)))
(comment (quit! driver))
